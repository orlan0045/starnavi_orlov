from django.shortcuts import render, redirect, HttpResponseRedirect
from django.contrib.auth import authenticate
from django.http import Http404, JsonResponse
from django.contrib.auth.hashers import make_password
from rest_framework import viewsets
from .models import User, Post, Likes
from .serializers import PostSerializer, User2Serializer
#PyHunter
from pyhunter import PyHunter
#JWT
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework import generics
import jwt
import task_rest.settings as settings
from rest_framework_jwt.utils import jwt_payload_handler
from rest_framework.decorators import api_view, permission_classes
from django.contrib.auth.signals import user_logged_in
from .forms import SignUpForm, PostForm

def index(request):
    last_posts = Post.objects.all().order_by('-created_at')[:3]
    form_post = PostForm()
    context = {
        'last_posts':last_posts,
        'form_post':form_post
    }
    return render(request, 'index.html', context)

#
# class UserView(viewsets.ModelViewSet):
#     queryset = User.objects.all()
#     serializer_class = UserSerializer

class PostView(viewsets.ModelViewSet):
    queryset = Post.objects.all()
    serializer_class = PostSerializer


class CreateUserAPIView(APIView):
    # Allow any user (authenticated or not) to access this url
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        user = request.data
        serializer = User2Serializer(data=user)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)


class UserRetrieveUpdateAPIView(generics.RetrieveAPIView):
    # Allow only authenticated users to access this url
    permission_classes = (IsAuthenticated,)
    serializer_class = User2Serializer

    def get(self, request, *args, **kwargs):
        # serializer to handle turning our `User` object into something that
        # can be JSONified and sent to the client.
        serializer = self.serializer_class(request.user)

        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, *args, **kwargs):
        serializer_data = request.data.get('user', {})

        serializer = User2Serializer(
            request.user, data=serializer_data, partial=True
        )
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(serializer.data, status=status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes([AllowAny, ])
def authenticate_user(request):
    try:
        email = request.data['email']
        password = request.data['password']
        user = authenticate(email=email, password=password)
        # user = User.objects.get(email=email, password=password)
        if user:
            try:
                payload = jwt_payload_handler(user)
                token = jwt.encode(payload, settings.SECRET_KEY)
                user_details = {}
                user_details['name'] = "%s %s" % (
                    user.first_name, user.last_name)
                user_details['token'] = token
                user_logged_in.send(sender=user.__class__,
                                    request=request, user=user)
                return Response(user_details, status=status.HTTP_200_OK)

            except Exception as e:
                raise e
        else:
            res = {
                'error': 'can not authenticate with the given credentials or the account has been deactivated'}
            return Response(res, status=status.HTTP_403_FORBIDDEN)
    except KeyError:
        res = {'error': 'please provide an email and a password'}
        return Response(res)



def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            # print('do')
            form.save()
        return HttpResponseRedirect('/accounts/login')
    else:
        form = SignUpForm()
        return render(request, 'registration/signup.html', {'form': form})

def checkmail(request):
    # PyHunter here
    if request.is_ajax():
        email = request.GET.get('email')
        hunter = PyHunter('9b9d3f0b966e09a3c1698a3f3f1789a8b758d34d')
        veri_email = hunter.email_verifier(email)
        status = veri_email['result']
        # undeliverable
        # risky
        response = {'status': status}
        return JsonResponse(response)
    else:
        raise Http404

def create_post(request):
    if request.POST:
        form = PostForm(request.POST, request.FILES)
        if form.is_valid():
            new_post = form.save(commit=False)
            new_post.author = request.user
            new_post.save()
            return redirect('/')
    else:
        form_post = PostForm()
        context = {
            'form':form_post
        }
        return render(request, 'create_post.html', context)

def post_page(request, post_id):
    post = Post.objects.get(id=post_id)
    likes = Likes.objects.filter(post_ref_id=post_id, like=True).count()
    dislikes = Likes.objects.filter(post_ref_id=post_id, like=False).count()
    context = {
        'post':post,
        'likes':likes,
        'dislikes':dislikes
    }
    return render(request, 'post_page.html', context)

def like(request, post_id):
    post_like, created = Likes.objects.get_or_create(post_ref_id=post_id, user_ref=request.user)
    post_like.like = True
    post_like.save()
    return redirect('/post/'+str(post_id))

def dislike(request, post_id):
    post_like, created = Likes.objects.get_or_create(post_ref_id=post_id, user_ref=request.user)
    post_like.like = False
    post_like.save()
    return redirect('/post/'+str(post_id))
