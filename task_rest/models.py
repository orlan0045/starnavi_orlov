from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import UserManager
from django.utils import timezone
from django.contrib.auth.models import (
    AbstractBaseUser, PermissionsMixin
)


class User(AbstractBaseUser, PermissionsMixin):
    """
    An abstract base class implementing a fully featured User model with
    admin-compliant permissions.

    """
    email = models.EmailField(max_length=40, unique=True)
    username = models.CharField(max_length=40, unique=True, blank=True)
    first_name = models.CharField(max_length=30, blank=True)
    last_name = models.CharField(max_length=30, blank=True)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    date_joined = models.DateTimeField(default=timezone.now)

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['first_name', 'last_name', 'username']

    def save(self, *args, **kwargs):
        super(User, self).save(*args, **kwargs)
        return self

def user_directory_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT/user_<id>/<filename>
    return 'user_{0}/{1}'.format(instance.author.id, filename)

class Post(models.Model):
    title = models.CharField(max_length=50)
    body = models.TextField(verbose_name='Зміст')
    photo = models.FileField(upload_to=user_directory_path, verbose_name='Файл', null=True)
    author = models.ForeignKey(User, verbose_name='автор', null=True, on_delete=models.SET_NULL)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title

class Likes(models.Model):
    post_ref = models.ForeignKey(Post, null=False, on_delete=models.CASCADE)
    user_ref = models.ForeignKey(User, null=True, on_delete=models.SET_NULL)
    like = models.BooleanField(null=False, default=True, verbose_name='Лайк?')
    created_at = models.DateTimeField(auto_now_add=True)