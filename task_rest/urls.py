"""task_rest URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from django.urls import path, include
from .views import *
from .random_attrs import *
from rest_framework import routers
#JWT

router = routers.DefaultRouter()
# router.register('users', UserView)
router.register('posts', PostView)

urlpatterns = [
    path('', index),
    path('accounts/', include('django.contrib.auth.urls')),
    path('accounts/signup/', signup, name='signup'),
    path('api/', include(router.urls)),
    path('admin/', admin.site.urls),
    #JWT
    path('api/create/', CreateUserAPIView.as_view()),
    path('api/update/', UserRetrieveUpdateAPIView.as_view()),
    path('api/token/', authenticate_user),
    #Python Hunter
    path('checkmail/', checkmail),
    #Posts
    path('createpost/', create_post, name='create_post'),
    path('post/<int:post_id>/', post_page, name='post_page'),
    path('like/<int:post_id>/', like, name='like'),
    path('dislike/<int:post_id>/', dislike, name='dislike'),
    #automated bot
    path('random/', random_it, name='random_it'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

