from django.shortcuts import  HttpResponseRedirect
from .models import User, Post, Likes
import random
import hashlib
from django.utils.crypto import get_random_string
import requests
from .random_config import number_of_users, max_likes_per_user, max_posts_per_user


def make_password(password):
    assert password
    hash = hashlib.md5(password.encode('utf-8')).hexdigest()
    return hash

def random_it(request):
    # ids = User.objects.all().values_list('id', flat=True)
    ids_posts = Post.objects.all().values_list('id', flat=True)
    #users
    for z in range(0, random.randint(1, number_of_users), 1):
        first_name = ('%06x' % random.randrange(16 ** 6)).upper()
        last_name = ('%06x' % random.randrange(16 ** 6)).upper()
        username = ('%06x' % random.randrange(16 ** 6))
        email = get_random_string(length=6) + '@random.it'
        User.objects.create(
            first_name=first_name,
            last_name=last_name,
            username=username,
            email=email,
            password=make_password('password')
        ).save()
    for u in User.objects.all():
        # posts
        for x in range(0,random.randint(1,max_posts_per_user),1):
            body = requests.get('https://baconipsum.com/api/?type=all-meat&paras=2&start-with-lorem=0&format=text')
            body_text = body.text
            t_request = requests.get('https://baconipsum.com/api/?type=all-meat&sentences=1&start-with-lorem=0&format=text')
            title = t_request.text[:12]
            author = u
            Post.objects.create(
                title = title,
                body = body_text,
                author = author
            ).save()
        # likes
        for y in range(0,random.randint(1, max_likes_per_user),1):
            user = u
            post = random.choice(ids_posts)
            Likes.objects.create(
                post_ref_id=post,
                user_ref=user
            ).save()
    return HttpResponseRedirect('/')

