from django import forms
from django.contrib.auth.forms import UserCreationForm
from .models import User, Post

class SignUpForm(UserCreationForm):
    first_name = forms.CharField(max_length=30, required=False, help_text='Optional.', widget=forms.TextInput(attrs={'placeholder':"Ім'я"}))
    last_name = forms.CharField(max_length=30, required=False, help_text='Optional.', widget=forms.TextInput(attrs={'placeholder':'Прізвище'}))
    email = forms.EmailField(max_length=254, help_text='Required. Inform a valid email address.', widget=forms.TextInput(attrs={'placeholder':'Email'}))

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'password1', 'password2')
        widgets = {
            'username': forms.TextInput(attrs={'placeholder':'Нік користувача'}),
        }

    def __init__(self, *args, **kwargs):
        super(SignUpForm, self).__init__(*args, **kwargs)
        self.fields['password1'].widget = forms.PasswordInput(attrs={'placeholder': "Пароль"})
        self.fields['password2'].widget = forms.PasswordInput(attrs={'placeholder': "Повторіть пароль"})

class PostForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = ('title', 'body', 'photo')
        widgets = {
            'title': forms.TextInput(attrs={'class':'form-control'}),
            'body': forms.Textarea(attrs={'cols': 80, 'rows': 4, 'class':'form-control'}),
        }